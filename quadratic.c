#include <stdio.h>
#include <conio.h>
#include <math.h>

void main(){
    float a, b, c, d, root1, root2, real, img;
    printf("\nTo find the root of a quadratic equation...");
    printf("\n\tEnter the coefficient of a, b, & c:");
    scanf("%f %f %f", &a, &b, &c);

    d = b*b-(4*a*c);

    if(d>0){
        printf("roots are real and distinct");
        root1 = (-b+sqrt(d))/(2*a);
        root2 = (-b-sqrt(d))/(2*a);
        printf("\n\t root1 = %.2f and root2 = %.2f", root1, root2);
    }
    else if (d==0){
        printf("roots are real and equal");
        root1 = root2 = -b/(2*a);
        printf("\n\t root1 = root2 = %.2f", root1);
    }
    else{
        printf("roots are imaginary");
        real = -b/(2*a);
        img = sqrt(4*a*c-b*b);
        printf("\n\t root1 = %.2f+%.2fi and root2 = %.2f-%.2fi", real, img, real, img);
    }
    getch();
}   
