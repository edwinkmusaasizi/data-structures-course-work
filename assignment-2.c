#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int number;
    struct Node *next;
};
struct Node* createNode(int num) {
    struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));
    if (newNode == NULL) {
        printf("Error: Unable to allocate memory for a new node\n");
        exit(1);
    }

    newNode->number = num;
    newNode->next = NULL;
    return newNode;
}

// Function to add a node at the end of the list
void append(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    if (*head == NULL) {
        *head = newNode;
        return;
    }
    struct Node *current = *head;
    while (current->next != NULL) {
        current = current->next;
    }
    current->next = newNode;
}

// Function to add a node at the beginning of the list
void prepend(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;
}


// Function to print all nodes in the list
void printList(struct Node *head) {
    struct Node *current = head;
    if (head == NULL){
        printf("The list is Empty\n");
    } else {
        printf("[");
        while (current != NULL) {
            printf("%d", current->number);
            current = current->next;

            if(current != NULL){
                printf(", ");
            }
        }
        printf("]\n");
    }
    
}

// Function to delete a Node by a Key
void deleteByKey(struct Node **head, int key){
    struct Node* current= *head;
    struct Node* prev= *head;

    if(*head == NULL){
        printf("The list empty");
    }else if (key == 1) {
        *head = current->next;
        free(current);
    } else{
        while(key != 1){
            prev = current;
            current = current->next;
            key--;
        }
        prev->next = current->next;
        free(current);
    }     
    
}

void deleteByValue(struct Node **head, int value){ 
    struct Node *current = *head;   
    struct Node *prev;    

    
    if(*head == NULL){
        printf("The list is Empty");
    }
    else if(current->number == value){
        *head = current->next;
        free(current);        
    }    
    else{
        while (current->number != value){
            prev = current;
            current = current->next;
        } 
        prev->next = current->next;
        free(current);
    }    

    
}

// Function that inserts a node after a given key(position)
void insertAfterKey(struct Node **head, int key, int value){
    struct Node *newNode = createNode(value);
    struct Node *current = *head;
    key--;
    while(key != 1){
        current = current->next;
        key--;
    }
    newNode->next = current->next;
    current->next = newNode;
}

// Function that inserts anode after a given Value
void insertAfterValue(struct Node **head, int searchValue, int newValue){
    struct Node *newNode = createNode(newValue);
    struct Node *current = *head;    

    while (current->number != searchValue){
        current = current->next;        
    }
    newNode->next =current->next;
    current->next = newNode;  

}


int main()
{
    struct Node *head = NULL;
    int choice, data, choice2, data2, key2, choice3, key3, data3, searchValue, newValue;    
    

    while (1)
    {
        printf("Linked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete\n");
        printf("5. Insert\n");
        printf("6. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice){
            case 1:
                printList(head);
                break;

            case 2:
                printf("Enter data to append: ");
                scanf("%d", &data);
                append(&head, data);                
                break;
            
            case 3:
                printf("Enter data to prepend: ");
                scanf("%d", &data);
                prepend(&head, data);                
                break;

            case 4:                
                printf("Enter (1) to delete by key and (2) to delete by Value: \n ");                
                scanf("%d", &choice2);
                switch(choice2){
                    case 1:
                        printf("Enter key whose data is to be deleted: ");
                        scanf("%d", &key2);
                        deleteByKey(&head, key2);                        
                        break;

                    case 2:
                        printf("Enter value t be deleted: ");
                        scanf("%d", &data2);
                        deleteByValue(&head, data2);                        
                        break;
                } 
                break;

            case 5:
                printf("Enter (1) to insert data at a given key(position) and (2) to insert data after given Value: \n ");                
                scanf("%d", &choice3);
                switch(choice3 ){
                    case 1:                        
                        printf("Enter key where data is be inserted: ");
                        scanf("%d", &key3);
                        printf("Enter the data to insert: ");
                        scanf("%d", &data3);
                        insertAfterKey(&head, key3, data3);                        
                        break;

                    case 2:                        
                        printf("Enter searchValue: ");
                        scanf("%d", &searchValue);
                        printf("Enter the newValue to be inserted: ");
                        scanf("%d", &newValue);
                        insertAfterValue(&head, searchValue, newValue);                        
                        break;

                } 
                break;

            case 6:                
                exit(1);
                break;

            default:
                printf("Invalid choice. Please try again.\n");
        }
    }

    return 0;
}
